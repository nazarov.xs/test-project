import { BaseWidget } from "./base-widget.js";

const WRAPPER_SELECTOR = ".js-filter-widget-wrapper";

const FILTER_TEMPLATE = (controls) => `
	<div class='filter-wrapper js-filter-widget-wrapper'>
		${controls
      .map(
        ({ name, type, label }) =>
          `
					<div class='filter-control'>
						<label for='${name}'>${label}</label>
							<input id='${name}' type='${type}' name='${name}'  />
					</div>`
      )
      .join("")}
      <div class='filter-clear-btn'>
        <button>Очистить</button>
      </div>
	</div>
	
`;

export class FilterWidget extends BaseWidget {
  initWidget(settings) {
    this.callbacks = [];
    this.controls = settings.controls;
    this.filter = new Map([]);

    this.data = settings.data;
  }

  initialRender() {
    this.rootWrapper = document.createElement("div");
    this.rootWrapper.innerHTML = FILTER_TEMPLATE(this.controls);
    this.inputs = this.rootWrapper.querySelectorAll("input");
    this.inputs.forEach((input) =>
      input.addEventListener("input", (e) => this.handleChangeFilter(e))
    );

    this.rootWrapper.querySelector("button").addEventListener("click", () => {
      this.filter.clear();
      this.inputs.forEach((input) => (input.value = null));
      this.filterData();
    });

    this.rootElement.append(this.rootWrapper);
  }

  subscribe(callback) {
    this.callbacks.push(callback);
  }

  unsubscribe(callback) {
    this.callbacks = this.callbacks.filter((x) => x !== callback);
  }

  handleChangeFilter(e) {
    const { name, value } = e.target;
    if (!value || !value.trim()) this.filter.delete(name);
    else this.filter.set(name, value.trim().toLowerCase());
    this.filterData();
  }

  filterData() {
    const filters = this.controls
      .filter((control) => this.filter.has(control.name))
      .map((control) => ({
        handler: control.filter,
        value: this.filter.get(control.name),
      }));

    const newData = filters.reduce(
      (filteredData, filter) => filter.handler(filteredData, filter.value),
      this.data
    );

    this.callbacks.forEach((callback) => {
      callback(this.filter, newData);
    });
  }
}
