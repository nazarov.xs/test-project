const JS_WRAPPER_CLASS = "js-widget-wrapper";
const JS_WRAPPER_SELECTOR = `.${JS_WRAPPER_CLASS}`;
const JS_LIST_CLASS = "js-list-widget-list";
const JS_LIST_SELECTOR = `.${JS_LIST_CLASS}`;

const WRAPPER_TEMPLATE = `
<div class='data-view-wrapper ${JS_WRAPPER_CLASS}'>
<ul class='${JS_LIST_CLASS}'></ul>
</div>
`;

const LIST_ITEM_TEMPLATE = (item) =>
  `<li>${item.map((x) => `<p>${x}</p>`).join("")}</li>`;

export class ListDataView {
  constructor({ listItemTemplate = LIST_ITEM_TEMPLATE }) {
    Object.assign(this, { listItemTemplate });
  }

  init(rootElement) {
    this.rootWrapper = document.createElement("div");
    this.rootWrapper.innerHTML = WRAPPER_TEMPLATE;
    this.listContainer = this.rootWrapper.querySelector(JS_LIST_SELECTOR);
    rootElement.append(this.rootWrapper);
  }
  render(data) {
    const items = data.map(this.listItemTemplate).join("");
    this.listContainer.innerHTML = items;
  }
}
