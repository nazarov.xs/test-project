import { BaseWidget } from "../base-widget.js";

export class DataViewWidget extends BaseWidget {
  set data(newData) {
    this._data = [...newData.map((x) => [...x])];
  }
  get data() {
    return this._data;
  }

  initWidget({ data, viewRender, ...renderSettigns }) {
    this.data = data;
    this.renderView = new viewRender(renderSettigns);
  }

  initialRender() {
    this.renderView.init(this.rootElement);
    this.renderView.render(this.data);
  }

  updateData(newData) {
    this.data = newData;
    this.renderView.render(this.data);
  }
}
