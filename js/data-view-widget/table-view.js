const JS_WRAPPER_CLASS = "js-widget-wrapper";
const JS_WRAPPER_SELECTOR = `.${JS_WRAPPER_CLASS}`;
const JS_TABLE_HEAD_ROW_CLASS = "js-table-widget-head-row";
const JS_TABLE_HEAD_ROW_SELECTOR = `.${JS_TABLE_HEAD_ROW_CLASS}`;
const JS_TABLE_BODY_CLASS = "js-table-widget-body";
const JS_TABLE_BODY_SELECTOR = `.${JS_TABLE_BODY_CLASS}`;

const WRAPPER_TEMPLATE = `
<div class='data-view-wrapper ${JS_WRAPPER_CLASS}'>
<table>
        <thead><tr class='${JS_TABLE_HEAD_ROW_CLASS}' /></thead>
        <tbody class='${JS_TABLE_BODY_CLASS}'></tbody>
    </table>
</div>
`;

const TABLE_HEADER_COLUMN_TEMPLATE = ({ label }) => `<th>${label}</th>`;

const TABLE_BODY_ROW_TEMPLATE = (row, columns, columnTemplate) =>
  `<tr>${row
    .map((col, index) => columnTemplate(col, columns[index].type))
    .join("")}</tr>`;

const TABLE_BODY_COLUMN_TEMPLATE = (column, type) => {
  return `<td>${
    type !== "unixtimestamp"
      ? column
      : new Date(column * 1000).toLocaleDateString()
  }</td>`;
};

const EMPTY_MESSAGE = (colsCount) => `
  <tr>
    <td colSpan=${colsCount} class='table-widget-empty'>По вашему запросу ничего не найдено</td>
  </tr>
`;

export class TableDataView {
  constructor({
    columns,
    headerColumnTemplate = TABLE_HEADER_COLUMN_TEMPLATE,
    bodyRowTemplate = TABLE_BODY_ROW_TEMPLATE,
    bodyColumnTemplate = TABLE_BODY_COLUMN_TEMPLATE,
  }) {
    if (!columns || !columns.length)
      throw new Error("TableDataView required set columns param");
    Object.assign(this, {
      columns,
      headerColumnTemplate,
      bodyRowTemplate,
      bodyColumnTemplate,
    });
  }

  init(rootElement) {
    this.rootWrapper = document.createElement("div");
    this.rootWrapper.innerHTML = WRAPPER_TEMPLATE;
    this.tableHeaderRow = this.rootWrapper.querySelector(
      JS_TABLE_HEAD_ROW_SELECTOR
    );
    this.tableBody = this.rootWrapper.querySelector(JS_TABLE_BODY_SELECTOR);
    rootElement.append(this.rootWrapper);
  }

  renderHead() {
    const headerColumns = this.columns.map(this.headerColumnTemplate).join("");
    this.tableHeaderRow.innerHTML = headerColumns;
  }

  renderBody(data) {
    if (!data.length) {
      this.tableBody.innerHTML = EMPTY_MESSAGE(this.columns.length);
      return;
    }
    const bodyRows = data
      .map((row) =>
        this.bodyRowTemplate(row, this.columns, this.bodyColumnTemplate)
      )
      .join("");
    this.tableBody.innerHTML = bodyRows;
  }

  render(data) {
    this.renderHead();
    this.renderBody(data);
  }
}
