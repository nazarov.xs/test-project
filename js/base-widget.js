export class BaseWidget {
  constructor(settings) {
    this.rootElement = document.querySelector(settings.selector);
    if (!this.rootElement)
      throw new Error(
        `Root element for widget with selector '${settings.selector}' not found`
      );
    this.initWidget(settings);
    this.initialRender();
  }
}
