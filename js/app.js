import { FilterWidget } from "./filter-widget.js";
import { DataViewWidget } from "./data-view-widget/data-view-widget.js";
import { TableDataView } from "./data-view-widget/table-view.js";
import { ListDataView } from "./data-view-widget/list-view.js";

async function initPage() {
  const response = await fetch("data.json");
  const body = await response.json();

  const filterWidget1 = new FilterWidget({
    selector: ".js-filter-widget1",
    data: body.data,
    controls: [
      {
        label: "Id",
        name: "id",
        type: "text",
        filter: (data, filterValue) =>
          data.filter((x) => x[0].toLowerCase().includes(filterValue)),
      },
      {
        label: "Название",
        name: "name",
        type: "text",
        filter: (data, filterValue) =>
          data.filter((x) => x[1].toLowerCase().includes(filterValue)),
      },
      {
        label: "Категория",
        name: "category",
        type: "text",
        filter: (data, filterValue) =>
          data.filter((x) => x[2].toLowerCase().includes(filterValue)),
      },
      {
        label: "Дата",
        name: "date",
        type: "date",
        filter: (data, filterValue) => {
          const date = new Date(filterValue);
          date.setHours(0);
          const timeStamp = date.getTime() / 1000;
          return data.filter((x) => x[3] === timeStamp);
        },
      },
      {
        label: "Значение",
        name: "value",
        type: "number",
        filter: (data, filterValue) =>
          data.filter((x) => x[4] === Number(filterValue)),
      },
    ],
  });

  const filterWidget2 = new FilterWidget({
    selector: ".js-filter-widget2",
    data: body.data,
    controls: [
      {
        label: "Название",
        name: "name",
        type: "text",
        filter: (data, filterValue) =>
          data.filter((x) => x[1].toLowerCase().includes(filterValue)),
      },
      {
        label: "Категория",
        name: "category",
        type: "text",
        filter: (data, filterValue) =>
          data.filter((x) => x[2].toLowerCase().includes(filterValue)),
      },
    ],
  });

  const tableWidget = new DataViewWidget({
    selector: ".js-table-widget",
    columns: body.columns,
    data: body.data,
    viewRender: TableDataView,
  });

  const listWidget = new DataViewWidget({
    selector: ".js-list-widget",
    data: body.data,
    viewRender: ListDataView,
    listItemTemplate: (item) =>
      `<li>${item.map((x) => `<span>${x}</span>`).join("")}</li>`,
  });

  const handleChangeFilter1 = (filter, data) => {
    tableWidget.updateData(data);
  };

  const handleChangeFilter2 = (filter, data) => {
    listWidget.updateData(data);
  };

  filterWidget1.subscribe(handleChangeFilter1);
  filterWidget2.subscribe(handleChangeFilter2);
}

initPage();
